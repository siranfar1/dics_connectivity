% ANOVA graph propertise only for scalar variables
% for other properties go to ANOVA_Graph2


% Just to see difference between pre and post separately



%%%%%%%%% LOADING  DATA Group 1 %%%%%%%%%%%%%%%%%%%%%%%%
post_graph_S1_open=load('Graph_post2_Session1_eyesopen.mat')
post_graph_S1_close=load('Graph_post2_Session1_eyesclosed.mat')
PRE_open_s1=load('Graph_baseline_Session1_eyesopen.mat')
PRE_close_s1=load('Graph_baseline_Session1_eyesclosed.mat')
inter_open_s1=load('Intercepts_Session1_eyesopen.mat')
inter_close_s1=load('Intercepts_Session1_eyesclosed.mat')

intercepts_G1=[inter_open_s1.intercepts  ;inter_close_s1.intercepts   ];

pre=[PRE_open_s1.pre_graph  ;PRE_close_s1.pre_graph  ];
post= [post_graph_S1_open.post_graph; post_graph_S1_close.post_graph];


G1_intercept= [intercepts_G1;intercepts_G1];

data_G1= [pre;post];
data_G1(:,2)=[]; % I found this variable to be 0, therefore I remove it
PRE_name= -1;
POST_name= 1;

NAME_G1= [repelem(PRE_name, size(pre,1))'; repelem( POST_name ,size(post,1))'];
Y_G1= [G1_intercept, NAME_G1];

%%%%%%%%%%%%%%%%%%%%%%%%%%%% session 2 %%%%%%%%%%%%%%%%%%%%%%%


post_graph_S0_open=load('Graph_post2_Session0_eyesopen.mat')
post_graph_S0_close=load('Graph_post2_Session0_eyesclosed.mat')
PRE_open_s0=load('Graph_baseline_Session0_eyesopen.mat')
PRE_close_s0=load('Graph_baseline_Session0_eyesclosed.mat')
inter_open_s0=load('Intercepts_Session0_eyesopen.mat')
inter_close_s0=load('Intercepts_Session0_eyesclosed.mat')

intercepts_G0=[inter_open_s0.intercepts  ;inter_close_s0.intercepts   ];

pre_G0=[PRE_open_s0.pre_graph  ;PRE_close_s0.pre_graph  ];
post_G0= [post_graph_S0_open.post_graph; post_graph_S0_close.post_graph];

pre_data_G0=[pre_G0];
post_data_G0=[post_G0];

data_G0= [pre_data_G0;post_data_G0];
data_G0(:,2)=[]; % I found this variable to be 0, therefore I remove it

NAME_G0= [repelem(PRE_name, size(pre,1))'; repelem( POST_name ,size(post,1))'];
G0_intercept= [intercepts_G0;intercepts_G0];
Y_G0= [G0_intercept, NAME_G0];

%%%%%%%%%%%%%% combining 2 groups of data %%%%%%%%%%%%%%

DATA_graph= [data_G1 ;data_G0];
Y= [Y_G1;Y_G0];


%%%%%%%% ANOVA %%%%%%%%%
% I took the first variable of graph to see the effect of other factors on
% it

for i= 1:size(DATA_graph,2)
 pval(:,i) = anovan(DATA_graph(:,i),Y, 'varnames', {'experiment_time', 'eyes', 'gender', 'sham_real', 'pre_post'});
close;
end

good_idx= find(pval< 0.05);
correction= bonf_holm(pval(good_idx));
significant_p= find(correction < 0.05) 
which_p=pval(good_idx(significant_p));
where= good_idx(significant_p);
which_factor= rem(where, 5) % first factor at nodes 64 , 66 are affected by time
which_node=where/5

% performing the test

% t = table(NAME_G1,data_G1(:,1),data_G1(:,2),data_G1(:,3),data_G1(:,4),data_G1(:,5),data_G1(:,6),data_G1(:,7),data_G1(:,8),data_G1(:,9), ...
% 'VariableNames',{'pre_post_G1','efficiency','path_length','global_efficiency','radius', 'diameter','experiment_time','eyes','gender','sham_real'});
% Meas = table([1 2 3 4 5  6  7 8 9]','VariableNames',{'Measurements'});
% 
% 
% rm = fitrm(t,'efficiency-sham_real~pre_post_G1','WithinDesign',Meas);
% ranovatbl = ranova(rm)
% 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 


