

import mne
import conpy
from mne.externals.h5io import write_hdf5
import sys
sys.path.insert(1, 'Scripts')
import Conn_utils
from operator import add
from config import subjects, eyes, bands, subjects_fs2meg, main, n_jobs, labels, subjects_dir
import os
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')

 #################### for permutation you can't paecelate in advance#################


group=1# Chose betzeen 1 or 0, I don't know which one is real or sham :)

eye  ='closed'
band =  'Low_Alpha'
cons1 = []
cons2 = []
cons3 = []

fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=main+'freesurfer', n_jobs=n_jobs, add_dist=False)


    
for subject in subjects:
    subjects_fs2meg.keys()
    meg_subject  = subjects_fs2meg[subject]
    
    try:
    
        con1_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','Baseline'  , group, eye, band))
        con1_fsaverage = con1_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
        #con1_parc = con1_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        cons1.append(con1_fsaverage)
        
        con2_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','postexpo1'  , group, eye, band))
        con2_fsaverage = con2_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
        #con2_parc = con2_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        cons2.append(con2_fsaverage)
        
        con3_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','postexpo2'  , group, eye, band))
        con3_fsaverage = con3_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
        #con3_parc = con3_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        cons3.append(con3_fsaverage)
        
    except:
        print('SUBJECT %s is missing' %subject)        
        
    print('for group %s subject %s is ok' %(group, subject))
    # Average the connection objects. To save memory, we add the data in-place.
print('Averaging connectivity objects...')


CON1= cons1[0].copy
CON2= cons2[0].copy
CON3= cons3[0].copy

for other_con1, other_con2, other_con3  in zip(cons1[1:], cons2[1:], cons3[1:]):
    CON1 += other_con1
    CON2 += other_con2
    CON3 += other_con3
     
     
'''
CON1 =  list( map(add, CON1,other_con1) )
CON2 =  list( map(add, CON2,other_con2) )
CON3 =  list( map(add, CON3,other_con3) )

# my data was like this, I faces an erorr then I changed it by using add function, THIS PART needs to be verified

'''
        
CON1 /= len(cons1)  # compute the mean
CON2 /= len(cons2) 
CON3 /= len(cons3) 

CON1.save('averaged connectivty of all subs_baseline_group_%s_eyes_%s_%s'  %( group, eye, band))
CON2.save('averaged connectivty of all subs_post1_group_%s_eyes_%s_%s'  %( group, eye, band))
CON3.save('averaged connectivty of all subs_post2_group_%s_eyes_%s_%s'  %( group, eye, band))
# Compute contrast between faces and scrambled pictures
#contrast1 = 
contrast2 = CON1 - CON2


#contrast1.save('connectivty difference between baseline_postexpo1__group_%s_eyes_%s_%s'  %(group, eye, band))
contrast2.save('connectivty difference between baseline_postexpo2__group_%s_eyes_%s_%s'  %(group, eye, band))


# Perform a permutation test to only retain connections that are part of a
# significant bundle.

CONN= dict()
CONN['Pre']=cons1
CONN['post']=cons2

stats = conpy.cluster_permutation_test(CONN['Pre']  , CONN['post'],
    cluster_threshold=5, src=fsaverage, n_permutations=1000, verbose=True,
    alpha=0.05, n_jobs=1, seed=10, return_details=True, max_spread=0.01)
connection_indices, bundles, bundle_ts, bundle_ps, H0 = stats
con_clust = contrast2[connection_indices]

# Save some details about the permutation stats to disk
write_hdf5('baseline_post2___group_%s_eyes_%s_%s'  %(group, eye, band), dict(
    connection_indices=connection_indices,
    bundles=bundles,
    bundle_ts=bundle_ts,
    bundle_ps=bundle_ps,
    H0=H0
), overwrite=True)



# Save the pruned grand average connection object
con_clust.save('pruned connectivty diff_baseline_post2___group_%s_eyes_%s_%s'  %(group, eye, band))

# Summarize the connectivity in parcels

del labels[-1]  # drop 'unknown-lh' label
con_parc = con_clust.parcellate(labels, summary='degree',
                                weight_by_degree=False)

con_parc.plot()

con_parc.save('parcellated pruned connectivty diff_baseline_post2___group_%s_eyes_%s_%s'  %(group, eye, band))

print('[done]')
