'''
 ANOVA test on imaginary connectity values
 '''
 
 
import mne
import numpy as np
import os 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/05_Scripts')
from config import eyes, subjects_dir, subjects_fs2meg, subjects
import Conn_utils
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh')


groups= [0, 1]
# Defining the parameters
labels = mne.read_labels_from_annot('fsaverage', parc='aparc', subjects_dir=subjects_dir)
del labels[-1]
labels_colors= [label.color for label in labels]
label_names = [label.name for label in labels]
band= 'Alpha'

cons1= []
cons2= []
cons3= []
eyecondition = []
sham_real = []
fre = []


for group in groups:
    
    for eye in eyes:
        
        for subject in subjects:
            subjects_fs2meg.keys()
            meg_subject  = subjects_fs2meg[subject]
        
            
            try:
                con1_subject = np.load(Conn_utils.Read_conn(meg_subject,'PLI', 'baseline' , group, eye, band)+'.npy')
                cons1.append(con1_subject)
                eyecondition.append(eye)
                sham_real.append(group)
                fre.append(band)
                
                
                con2_subject = np.load(Conn_utils.Read_conn(meg_subject, 'PLI', 'postexpo1' , group, eye, band)+'.npy')
                cons2.append(con2_subject)
            
                
                con3_subject = np.load(Conn_utils.Read_conn(meg_subject, 'PLI', 'postexpo2', group, eye, band)+'.npy')
                cons3.append(con3_subject)

            
            except:
                print('######subject %s is missing' %subject)

#with open("imCoh.txt","w") as f:
#    for (con) in zip(cons1,cons2,cons3,eyecondition,sham_real,fre):
#        f.write("{0},{1},{2},{3},{4},{5} \n".format(cons1,cons2,cons3,eyecondition,sham_real,fre))
           
G1= [cons1,cons2,cons3,eyecondition,sham_real,fre]            
np.save('PLI-ALL', G1)
'''
AVOVA Preparation
'''





''' 
for graph
'''

A=G1[0]
Ave = A[0].copy()

i=1
for con in range(1,26): # based on the logic I created cons1, the first 27 values of this list correspond to group0, eyes open
    Ave +=  A[con]  # I really don't know why some values re negative but If I take average like this, I come up with nothing almost 0, then I decided to take the absolute value
    i+=1

Ave= Ave/(i+1)


#effects = 'A:B'
#n_conditions = 4
#T_obs, clusters, cluster_p_values, H0 = clu =  mne.stats.permutation_cluster_test([G1[0],G1[1]],n_permutations=5000, verbose=True) #does not work
