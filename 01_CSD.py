'''
####################### THIS FILES IS ONLY FOR CSD GENERATION    ##############################
set your working directory
change parameters in the config.py based on your need
'''

import mne
import numpy as np
import conpy
import os 
import sys
sys.path.insert(1, 'Connectivity/Scripts')
from config import eyes, runs, min_pair_dist, subjects_dir, reg, n_jobs, titles, CON, max_sensor_dist, subjects_fs2meg
import Conn_utils

wd=os.getcwd()
if wd !='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF':
    os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF')

from mne.minimum_norm import read_inverse_operator, apply_inverse_raw
from mne.connectivity import spectral_connectivity 
from mne.viz import circular_layout, plot_connectivity_circle
from conpy import (dics_connectivity, all_to_all_connectivity_pairs, one_to_all_connectivity_pairs, forward_to_tangential)
from mne.time_frequency import csd_morlet
from mne.beamformer import make_dics, apply_dics_csd
from mne.time_frequency import read_csd, pick_channels_csd
from mayavi import mlab



subjects_fs2meg = {  
'2017_11_07_BRAIN_RF_01_CCOS':'brain_rf01_s01',
'2017_11_15_BRAIN_RF_03_ACHA':'brain_rf03_s03',
'2017_12_13_BRAIN_RF_06_YBUR':'brain_rf06_s06',
'2017_12_12_BRAIN_RF_07_RNOE':'brain_rf07_s07',
'2017_12_22_BRAIN_RF_08_MCUV':'brain_rf08_s08',
'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15',
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20',
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26',
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28',
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}


for subject in subjects_fs2meg:
    if not subject in subjects_fs2meg.keys() : raise Exception('fs_subject not found in subjects_fs2meg')
    subjects_fs2meg.keys()
    meg_subject  = subjects_fs2meg[subject]



    session=2
    
    fname= [ Conn_utils.raw_fname(meg_subject, session , run ) for run in runs]
    raw_run1  =mne.io.read_raw_fif(fname[0])
    raw_run2  =mne.io.read_raw_fif(fname[1])
    raw_run9  =mne.io.read_raw_fif(fname[2])
    raw_run10 =mne.io.read_raw_fif(fname[3])
    raw_run11 =mne.io.read_raw_fif(fname[4])
    raw_run12 =mne.io.read_raw_fif(fname[5])


    raw_pre= raw_run1.copy()
    raw_pre.append(raw_run2)
    
    raw_post1 =raw_run9.copy()
    raw_post1.append(raw_run10)
    
    raw_post2 =raw_run11.copy()
    raw_post2.append(raw_run12)


 

    info        = mne.io.read_info(fname[0])
    picks       = mne.pick_types(info, meg='grad', eeg= False)
    
        
    # reading epochs
    epochs_baseline , events_baseline  = Conn_utils.epoch_rf(raw_pre, picks=picks)
    epochs_postexpo1, events_postexpo1 = Conn_utils.epoch_rf(raw_post1, picks=picks)
    epochs_postexpo2, events_postexpo2 = Conn_utils.epoch_rf(raw_post2, picks=picks)

    # Ploting the results
#    epochs_postexpo1.plot(events=events_postexpo1)
#    epochs_postexpo1.plot_image( cmap='interactive', sigma=1.0)
#    raw_post1.copy().pick_types(meg=False, stim=True).plot()
#    mne.viz.plot_events(events_postexpo1)
#    
    
    for eye in eyes:
        
        csd_baseline   = Conn_utils.csd_epochs(epochs_baseline, eye) #for more info please visit Conn_utils   
        csd_postexpo1  = Conn_utils.csd_epochs(epochs_postexpo1, eye) #for more info please visit Conn_utils
        csd_postexpo2  = Conn_utils.csd_epochs(epochs_postexpo2, eye) #for more info please visit Conn_utils
        
        # This cross spectrum density is calculated for the frequency range of 7 to 25 Hz
        # I am going to separate each CSD of each frequency band
        
        
        # Baseline  
        csd_baseline_low_alpha     =csd_baseline.mean(8,10)
        csd_baseline_high_alpha    =csd_baseline.mean(10, 12)
#        csd_baseline_low_beta      =csd_baseline.mean(12, 16)
#        csd_baseline_high_beta     =csd_baseline.mean(16, 25)
        
        csd_baseline_low_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'Baseline', session, eye, 'Low_Alpha'))
        csd_baseline_high_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'Baseline', session, eye, 'High_Alpha'))

        
        # post exposition 1
        csd_postexpo1_low_alpha     =csd_postexpo1.mean(8,10)
        csd_postexpo1_high_alpha    =csd_postexpo1.mean(10, 12)

        
        csd_postexpo1_low_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo1', session, eye, 'Low_Alpha'))
        csd_postexpo1_high_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo1', session, eye, 'High_Alpha'))


         # post exposition 2
        csd_postexpo2_low_alpha     =csd_postexpo2.mean(8,10)
        csd_postexpo2_high_alpha    =csd_postexpo2.mean(10, 12)

        
        csd_postexpo2_low_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo2', session, eye, 'Low_Alpha'))
        csd_postexpo2_high_alpha.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo2', session, eye, 'High_Alpha'))

    print(' ______________ subject %s is DONE __________ ' %subject)