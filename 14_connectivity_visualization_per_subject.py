

import mne
import conpy
import matplotlib.pyplot as plt
import sys
sys.path.insert(1, '05_Scripts')
import Conn_utils
from config import subjects, eyes, bands, subjects_fs2meg, main, n_jobs, labels
import os
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')

 #################### for permutation you can't paecelate in advance#################
del labels[-1]
subjects_dir='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/freesurfer'

group=1# Chose betzeen 1 or 0, I don't know which one is real or sham :)
fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=main+'freesurfer', n_jobs=n_jobs, add_dist=False)
subject= '2017_11_15_BRAIN_RF_03_ACHA'
eye= 'closed'
band= 'High_Alpha'
#for eye in eyes:
#    
#    for band in bands:
#        
        
#        for subject in subjects:
subjects_fs2meg.keys()
meg_subject  = subjects_fs2meg[subject]

try:

    con1_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','Baseline'  , group, eye, band))
    con1_fsaverage = con1_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
  
    con2_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','postexpo1'  , group, eye, band))
    con2_fsaverage = con2_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)

    con3_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity','postexpo2'  , group, eye, band))
    con3_fsaverage = con3_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)

except:
    print('SUBJECT %s is missing' %subject)        



contrast1= con1_fsaverage - con2_fsaverage
contrast2= con1_fsaverage - con3_fsaverage


#            stats1 = conpy.cluster_permutation_test(con1_subject,con2_subject,
#                cluster_threshold=5, src=fsaverage, n_permutations=1000, verbose=True,
#                alpha=0.05, n_jobs=n_jobs, seed=10, return_details=True, max_spread=0.01)
#            connection_indices1, bundles1, bundle_ts1, bundle_ps1, H01 = stats1
#            con_clust1 = contrast1 [connection_indices1]
#            
#            stats2 = conpy.cluster_permutation_test(con1_subject,con3_subject,
#                cluster_threshold=5, src=fsaverage, n_permutations=1000, verbose=True,
#                alpha=0.05, n_jobs=n_jobs, seed=10, return_details=True, max_spread=0.01)
#            connection_indices2, bundles2, bundle_ts2, bundle_ps2, H02 = stats2
#            con_clust2 = contrast2 [connection_indices2]
#        
con_parc1 = contrast1.parcellate(labels, summary='degree', weight_by_degree=False)
con_parc2 = contrast2.parcellate(labels, summary='degree', weight_by_degree=False)

fig, _ = con_parc1.plot()
fig.savefig('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/%s_%s_%s_%s_contrast1'%(subject, eye, band, group), bbox_inches='tight')
fig1, _ = con_parc2.plot()
fig1.savefig('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/%s_%s_%s_%s_contrast2'%(subject, eye, band, group), bbox_inches='tight')
plt.close('all')