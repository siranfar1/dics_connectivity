import mne
import numpy as np
from mne.time_frequency import csd_morlet
from mne.beamformer import make_dics, apply_dics_csd
import os.path as op
import os




subjects_fs2meg = {
'2017_11_07_BRAIN_RF_01_CCOS':'brain_rf01_s01',
'2017_11_15_BRAIN_RF_03_ACHA':'brain_rf03_s03',
'2017_12_13_BRAIN_RF_06_YBUR':'brain_rf06_s06',
'2017_12_12_BRAIN_RF_07_RNOE':'brain_rf07_s07',
'2017_12_22_BRAIN_RF_08_MCUV':'brain_rf08_s08',
'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15',
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20',
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26',
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28',
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}

subjects_dir='freesurfer'
base_dir     = '/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF'
reg=0.05

def fs2meg( fs_subject ) :
    if not fs_subject in subjects_fs2meg.keys() : raise Exception('fs_subject not found in subjects_fs2meg')
    return subjects_fs2meg[fs_subject]
    

def meg2fs( meg_subject ) :
    if not meg_subject in subjects_fs2meg.values() : raise Exception('meg_subject not found in subjects_fs2meg')
    tmp = [ s for s in subjects_fs2meg if subjects_fs2meg[s] == meg_subject ]
    assert len(tmp) == 1
    return tmp[0]


def epoch_rf(raw, picks, event_dict={'open':3,'close':5}):
    events = mne.find_events(raw, output='step') 
    epoch = mne.Epochs(raw, events ,tmax=180, picks=picks, event_id=event_dict,baseline=(None, 0), reject=dict(grad=4000e-13), preload=True)
    return epoch, events

    

    
    
def csd_epochs(epoch, eyes, reg=0.05, pick_ori='max-power', n_cycle=7):
    
    fmin=8
    fmax=12
    frequency= np.linspace(fmin, fmax, num= fmax-fmin+1)
    
    if eyes=='open': 
        epoch = epoch['open'].apply_baseline()
        csd_epoch= csd_morlet(epoch, frequencies= frequency, n_cycles=7)

    elif eyes=='closed': 
        epoch = epoch['close'].apply_baseline()
        csd_epoch= csd_morlet(epoch, frequencies= frequency, n_cycles=7) #, decim=1, n_jobs=16
#        

    return csd_epoch

    


def stc_csd(info, csd, eyes, band, fwd):
    stc=[]
    if band=='Low_Alpha':
        fmin,fmax= 8, 10
      
    elif band =='High_Alpha':
        fmin,fmax= 10, 12

    if eyes=='open':  
#        csd=csd.mean(fmin, fmax)
        fillter =make_dics(info, fwd, csd, reg=0.05, pick_ori='max-power') # Check regularization, it must be around 0.05
        stc, f= apply_dics_csd(csd, fillter) 
        
    elif eyes=='closed': 
#        csd=csd.mean(fmin, fmax)
        fillter =make_dics(info, fwd, csd,reg=0.05 , pick_ori='max-power')
        stc, f = apply_dics_csd(csd, fillter) 
        
    return stc



def CSD_fname(meg_subject, CON, expo, session, eye, band):        
      fname =  "%s__%s__session%d__%s__eyes%s__%s" %(meg_subject, CON, session,expo, eye, band)
      return fname

    
def Read_conn(meg_subject, CON, expo, group, eye, band):
    
        if meg_subject == 'brain_rf01_s01' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf03_s03' :
            if group == 1 : 
                 session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf06_s06' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf07_s07' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
        
        elif meg_subject == 'brain_rf08_s08' :
            if group == 1 : 
                 session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf09_s09' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf10_s10' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf11_s11' :
            if group == 1 :
                 session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf12_s12' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf13_s13' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf14_s14' :
            if group == 1 : 
                 session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf15_s15' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf16_s16' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf17_s17' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf18_s18' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf19_s19' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf20_s20' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf21_s21' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf22_s22' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf23_s23' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf24_s24' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf25_s25' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf26_s26' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf27_s27' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf28_s28' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf29_s29' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf30_s30' :
            if group == 1 : 
                session= 1
            elif group== 0:
                session= 2
                
        elif meg_subject == 'brain_rf31_s31' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        elif meg_subject == 'brain_rf32_s32' :
            if group == 1 : 
                session= 2
            elif group== 0:
                session= 1
                
        fname =  "%s__%s__session%d__%s__eyes%s__%s" %(meg_subject, CON, session,expo, eye, band)
        return fname
                
                
                
                
                

                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
        
  
def raw_fname( meg_subject, session, run ) :
    sess_date = session_date( meg_subject, session )
    if run == 1 :
        run_id  = 'run01_baseline1'
    elif run == 2 :
        run_id  = 'run02_baseline2'
    elif run == 9 :
        run_id  = 'run09_post_exposition1'
    elif run == 10 :
        run_id  = 'run10_post_exposition2'
    elif run == 11 :
        run_id  = 'run11_post_exposition3'
    elif run == 12 :
        run_id  = 'run12_post_exposition4'
    fname = os.path.join( meg_subject, sess_date, run_id + '_trans_tsss_ica_eye_cardio_corrected_mne_EEG029_039.fif' )
    if not os.path.exists(fname): raise Exception('%s not found'%fname)
    return fname




def session_date( meg_subject, session ) :
    if meg_subject == 'brain_rf01_s01' :
        if session == 1 : 
            sess_date = '171030'
        elif session == 2 : 
            sess_date = '171106'

    elif meg_subject == 'brain_rf03_s03' :
        if session == 1 : 
            sess_date = '171102'
        elif session == 2 : 
            sess_date = '171109'

    elif meg_subject == 'brain_rf06_s06' :
        if session == 1 : 
            sess_date = '171129'
        elif session == 2 : 
            sess_date = '171208'

    elif meg_subject == 'brain_rf07_s07' :
        if session == 1 : 
            sess_date = '171130'
        elif session == 2 : 
            sess_date = '171207'

    elif meg_subject == 'brain_rf08_s08' :
        if session == 1 : 
            sess_date = '171211'
        elif session == 2 : 
            sess_date = '171218'
            
    elif meg_subject == 'brain_rf09_s09' :
        if session == 1 : 
            sess_date = '171212'
        elif session == 2 : 
            sess_date = '171219'

    elif meg_subject == 'brain_rf10_s10' :
        if session == 1 : 
            sess_date = '171213'
        elif session == 2 : 
            sess_date = '171220'

    elif meg_subject == 'brain_rf11_s11' :
        if session == 1 : 
            sess_date = '180123'
        elif session == 2 : 
            sess_date = '180130'

    elif meg_subject == 'brain_rf12_s12' :
        if session == 1 : 
            sess_date = '180124'
        elif session == 2 : 
            sess_date = '180131'

    elif meg_subject == 'brain_rf13_s13' :
        if session == 1 : 
            sess_date = '180125'
        elif session == 2 : 
            sess_date = '180201'

    elif meg_subject == 'brain_rf14_s14' :
        if session == 1 : 
            sess_date = '180205'
        elif session == 2 : 
            sess_date = '180212'

    elif meg_subject == 'brain_rf15_s15' :
        if session == 1 : 
            sess_date = '180206'
        elif session == 2 : 
            sess_date = '180213'

    elif meg_subject == 'brain_rf16_s16' :
        if session == 1 : 
            sess_date = '180207'
        elif session == 2 : 
            sess_date = '180214'

    elif meg_subject == 'brain_rf17_s17' :
        if session == 1 : 
            sess_date = '180226'
        elif session == 2 : 
            sess_date = '180305'

    elif meg_subject == 'brain_rf18_s18' :
        if session == 1 : 
            sess_date = '180301'
        elif session == 2 : 
            sess_date = '180308'

    elif meg_subject == 'brain_rf19_s19' :
        if session == 1 : 
            sess_date = '180302'
        elif session == 2 : 
            sess_date = '180309'

    elif meg_subject == 'brain_rf20_s20' :
        if session == 1 : 
            sess_date = '180306'
        elif session == 2 : 
            sess_date = '180313'

    elif meg_subject == 'brain_rf21_s21' :
        if session == 1 : 
            sess_date = '180315'
        elif session == 2 : 
            sess_date = '180322'

    elif meg_subject == 'brain_rf22_s22' :
        if session == 1 : 
            sess_date = '180320'
        elif session == 2 : 
            sess_date = '180327'

    elif meg_subject == 'brain_rf23_s23' :
        if session == 1 : 
            sess_date = '180328'
        elif session == 2 : 
            sess_date = '180404'

    elif meg_subject == 'brain_rf24_s24' :
        if session == 1 : 
            sess_date = '180329'
        elif session == 2 : 
            sess_date = '180405'

    elif meg_subject == 'brain_rf25_s25' :
        if session == 1 : 
            sess_date = '180403'
        elif session == 2 : 
            sess_date = '180410'

    elif meg_subject == 'brain_rf26_s26' :
        if session == 1 : 
            sess_date = '180406'
        elif session == 2 : 
            sess_date = '180410'

    elif meg_subject == 'brain_rf27_s27' :
        if session == 1 : 
            sess_date = '180416'
        elif session == 2 : 
            sess_date = '180420'

    elif meg_subject == 'brain_rf28_s28' :
        if session == 1 : 
            sess_date = '180517'
        elif session == 2 : 
            sess_date = '180524'

    elif meg_subject == 'brain_rf29_s29' :
        if session == 1 : 
            sess_date = '180522'
        elif session == 2 : 
            sess_date = '180529'

    elif meg_subject == 'brain_rf30_s30' :
        if session == 1 : 
            sess_date = '180530'
        elif session == 2 : 
            sess_date = '180607'

    elif meg_subject == 'brain_rf31_s31' :
        if session == 1 : 
            sess_date = '180604'
        elif session == 2 : 
            sess_date = '180611'

    elif meg_subject == 'brain_rf32_s32' :
        if session == 1 : 
            sess_date = '180703'
        elif session == 2 : 
            sess_date = '180710'

    else :
        raise Exception('subject %s not implemented in def session_date( meg_subject, session )'%meg_subject)
    return sess_date







def empty_room_fname( meg_subject, session ) :
    sess_date = session_date( meg_subject, session )
    fname = os.path.join(meg_subject, sess_date, 'empty_room_tsss.fif' )
    if not os.path.exists(fname): raise Exception('%s not found'%fname)
    return fname



def subject_empty_room_cov( meg_subject, session ) :
    fname   = op.join( meg_subject, 'SRC', '__'.join( [ meg_subject, 'session%d', 'empty_room' ] ) %(session) + '-cov.fif.gz' )
    if not op.exists(fname) :
        raw     = mne.io.read_raw_fif( empty_room_fname( meg_subject, session ) )
        cov     = mne.compute_raw_covariance(raw, tmin=0, tmax=None)
        cov.save( fname )
    else :
        print('Read',fname)
        cov     = mne.read_cov( fname )
    return cov

def bem_dir( fs_subject ) :
    bd = op.join(subjects_dir, fs_subject, 'bem')
    if not os.path.exists(bd) : os.mkdir(bd)    
    return bd



def subject_inv( meg_subject, session ) :
    fname = op.join(base_dir , meg_subject, 'SRC', meg_subject + '-inv.fif' )
    if not op.exists(fname) :
        info        = mne.io.read_info( op.join( meg_subject, 'subject_meanhp.fif' ) )
        fwd         = subject_fwd( meg2fs(meg_subject) )
        noise_cov   = subject_empty_room_cov( meg_subject, session )
        inv         = mne.minimum_norm.make_inverse_operator( info, fwd, noise_cov, loose='auto', depth=0.8, fixed='auto', limit_depth_chs=True, rank=None, use_cps=True, verbose=None)
        mne.minimum_norm.write_inverse_operator(fname, inv )
    else :
        print('Read',fname)
        inv         = mne.minimum_norm.read_inverse_operator( fname )
    return inv

def trans_fname( fs_subject ) :
    fname = op.join( fs2meg(fs_subject), 'SRC', fs_subject + '-trans.fif' )
    return fname


def src_fname( fs_subject ) :
    fname = op.join( bem_dir(fs_subject), '%s-oct-6-src.fif' % fs_subject)
    return fname



def subject_fwd( fs_subject ) :
   fsaverage    = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=subjects_dir,add_dist=False)
   src          = mne.morph_source_spaces(fsaverage, fs_subject, subjects_dir=subjects_dir)
   meg_subject  = fs2meg( fs_subject )
   fname        = op.join( base_dir, meg_subject, 'SRC', meg_subject+'-fwd.fif' )
   model        = mne.make_bem_model(subject=fs_subject, ico=4, conductivity=[0.3], subjects_dir=subjects_dir)
   bem          = mne.make_bem_solution(model)
   info         = mne.io.read_info( op.join( base_dir, meg_subject, 'subject_meanhp.fif' ) )
   trans        = trans_fname( fs_subject )
   fwd          = mne.make_forward_solution(info, trans=trans, src=src, bem=bem, eeg=False)
   
   return fwd


def read_fwd( fs_subject ) :
    meg_subject = fs2meg( fs_subject )
    fname       = op.join( base_dir, meg_subject, 'SRC', meg_subject+'-fwd.fif' )
    if op.exists(fname) :
        fwd     = mne.read_forward_solution(fname)
    else :
        model   = mne.make_bem_model(subject=fs_subject, ico=4, conductivity=[0.3], subjects_dir=subjects_dir)
        bem     = mne.make_bem_solution(model)
        info    = mne.io.read_info( op.join( base_dir, meg_subject, 'subject_meanhp.fif' ) )
        trans   = trans_fname( fs_subject )
        src     = mne.read_source_spaces( src_fname(fs_subject) )
        fwd     = mne.make_forward_solution(info, trans=trans, src=src, bem=bem, eeg=False)
        mne.write_forward_solution(fname, fwd )
    return fwd


def read_r_fwd( fs_subject ) :
    fname       = op.join( base_dir+"""/Connectivity/00_Restricted fwd""", """restricted_forward"""+"""_"""+ fs_subject + '-fwd.fif')
    fwd     = mne.read_forward_solution(fname)
        
    return fwd
