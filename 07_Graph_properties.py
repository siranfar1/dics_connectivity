#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compute Graph properties from a given connectivity matrix
"""


import mne
import conpy
from mne.externals.h5io import write_hdf5
import sys
sys.path.insert(1, 'Scripts')
import Conn_utils
from operator import add
from functools import reduce
from config import subjects, eyes, bands, subjects_fs2meg, main, n_jobs, labels,subjects_dir
import os
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')
mne.set_log_level('INFO')

fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=main+'freesurfer', n_jobs=n_jobs, add_dist=False)
conditions= ['Baseline', 'postexpo1', 'postexpo2']      
eyecondition= 'eyecondition'
freband= 'alphaband'
blind= 'blind'
pre = 'pre'
post1= 'post1'
post2= 'post2'

del labels[-1]
cons   = dict()
group = 0
cons[blind]= list()
cons[eyecondition]=list()
cons[freband]= list()
cons[pre]= list()
cons[post1]=list()
cons[post2]= list()
    
#
#for eye in eyes:
#    
#    for band in bands:
eye= 'open'
band= 'High_Alpha'
for subject in subjects:
    try:
        meg_subject= subjects_fs2meg[subject]
        con1_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[0] , group, eye, band))
        con2_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[1] , group, eye, band))
        con3_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[2] , group, eye, band))
    
        # Morph the Connectivity to the fsaverage brain. This is possible, since the original source space was fsaverage morphed to the current subject.
        con1_fsaverage = con1_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
#        con1_parc = con1_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        
        
        con2_fsaverage = con2_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
#        con2_parc = con2_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        
        con3_fsaverage= con3_subject.to_original_src(fsaverage, subjects_dir= subjects_dir)
#        con3_parc = con3_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
        
        # By now, the connection objects should define the same connection pairs between the same vertices.

        cons[pre].append(con1_fsaverage)
        cons[post1].append(con2_fsaverage)
        cons[post2].append(con3_fsaverage)
    except:
            print('subject %s is missing' %subject)
            
            
    conn1 = reduce(add,cons[pre] ) / len(cons[pre]) # this part is just to test 
       
        
    CON1= cons[pre][0]
    CON2= cons[post1][0]
    CON3= cons[post2][0]
    
    for other_con1, other_con2, other_con3  in zip(cons[pre][1:], cons[post1][1:],cons[post2][1:]):
        CON1 += other_con1
        CON2 += other_con2
        CON3 += other_con3
        
    CON1 /= len(cons[pre])  # compute the mean
    CON2 /= len(cons[post1]) 
    CON3 /= len( cons[post2]) 
    
    
    CON1_parc= CON1.parcellate(labels, summary='degree',weight_by_degree=False)                      
    CON2_parc= CON2.parcellate(labels, summary='degree',weight_by_degree=False)                      
    CON3_parc= CON3.parcellate(labels, summary='degree',weight_by_degree=False)                      
     

