"""
===========
Config file
===========
Configuration parameters for the study.
"""

import mne

base_dir     = '/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF'

max_sensor_dist = 0.07
session=1

subjects_fs2meg = {
'2017_11_07_BRAIN_RF_01_CCOS':'brain_rf01_s01',
'2017_11_15_BRAIN_RF_03_ACHA':'brain_rf03_s03',
'2017_12_13_BRAIN_RF_06_YBUR':'brain_rf06_s06',
'2017_12_12_BRAIN_RF_07_RNOE':'brain_rf07_s07',
'2017_12_22_BRAIN_RF_08_MCUV':'brain_rf08_s08',
'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15',
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20',
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26',
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28',
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}

subjects_dir='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/freesurfer'
subjects=list(subjects_fs2meg)
labels = mne.read_labels_from_annot('fsaverage', 'aparc', subjects_dir= subjects_dir)

conditions= {
        'Baseline':0,
        'postexpo1':1,
        'postexpo2':2}


main= '/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/'
base= '/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/Restricted fwd/restricted_forward'
subjects_dir='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/freesurfer'
CON='connectivity'

sfreq= 1000
# I restricted the frequency bands just to high and low alpha
bands= ['Low_Alpha', 'High_Alpha']


# set the range of the frequency you would like to study connectivty on
max_sensor_dist = 0.07
# experimental runs
runs= [1, 2, 9, 10, 11, 12]
titles= ['Contrast-baseline-postexposition1', 'Contrast-baseline-postexposition2', 'Contrast-postexposition1-postexposition2']
expositions= ['pre', 'postexpo1', 'postexpo2']


# eyes condition
eyes= ['open', 'closed']


# Minimum distance between sources to compute connectivity for (in meters)
min_pair_dist = 0.04
reg=0.05 # you can change the regularization value
n_jobs=16 # depending on the power of your machine, you can change the number of jobs to use more core
