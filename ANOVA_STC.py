#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 02:35:59 2020

@author: sepideh.iranfar
"""

# loading modules

import mne
import numpy as np
import os 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/05_Scripts')
from config import eyes, runs, subjects_dir, reg, n_jobs, max_sensor_dist, sfreq, bands, main 
import Conn_utils 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF')

 

# subjects 1 and 21 are ,missing so I remove them from analysis
subjects_fs2meg={
#         '2017_11_07_BRAIN_RF_01_CCOS': 'brain_rf01_s01',
 '2017_11_15_BRAIN_RF_03_ACHA': 'brain_rf03_s03',
 '2017_12_13_BRAIN_RF_06_YBUR': 'brain_rf06_s06',
 '2017_12_12_BRAIN_RF_07_RNOE': 'brain_rf07_s07',
 '2017_12_22_BRAIN_RF_08_MCUV': 'brain_rf08_s08',
 '2018_01_10_BRAIN_RF_09_BTOR': 'brain_rf09_s09',
 '2018_01_16_BRAIN_RF_10_MMAU': 'brain_rf10_s10',
 '2018_02_12_BRAIN_RF_11_PTUR': 'brain_rf11_s11',
 '2018_02_14_BRAIN_RF_12_COGO': 'brain_rf12_s12',
 '2018_02_01_BRAIN_RF_13_CFAV': 'brain_rf13_s13',
 '2018_02_14_BRAIN_RF_14_CBUT': 'brain_rf14_s14',
 '2018_03_09_BRAIN_RF_15_AMOR': 'brain_rf15_s15',
 '2018_02_28_BRAIN_RF_16_DMIC': 'brain_rf16_s16',
 '2018_04_04_BRAIN_RF_17_AAUJ': 'brain_rf17_s17',
 '2018_03_15_BRAIN_RF_18_CCLO': 'brain_rf18_s18',
 '2018_03_09_BRAIN_RF_19_LGIG': 'brain_rf19_s19',
 '2018_04_09_BRAIN_RF_20_PPEI': 'brain_rf20_s20',
# '2018_04_05_BRAIN_RF_21_SMNI': 'brain_rf21_s21',
 '2018_04_09_BRAIN_RF_22_JBEA': 'brain_rf22_s22',
 '2018_04_09_BRAIN_RF_23_MNYO': 'brain_rf23_s23',
 '2018_04_11_BRAIN_RF_24_SYAH': 'brain_rf24_s24',
 '2018_04_11_BRAIN_RF_25_GBER': 'brain_rf25_s25',
 '2018_04_11_BRAIN_RF_26_CRAT': 'brain_rf26_s26',
 '2018_05_03_BRAIN_RF_S27_NNAV': 'brain_rf27_s27',
 '2018_06_07_BRAIN_RF_S28_SPAI': 'brain_rf28_s28',
 '2018_06_11_BRAIN_RF_29_RMOR': 'brain_rf29_s29',
 '2018_06_21_BRAIN_RF_30_SBAS': 'brain_rf30_s30',
 '2018_06_21_BRAIN_RF_31_JFER': 'brain_rf31_s31',
 '2018_07_13_BRAIN_RF_32_EBEN': 'brain_rf32_s32'}

band= 'Alpha'
groups= [0,1]



G1_PRE_open=np.zeros((20484,180, 27 ))
G1_POST_open=np.zeros((20484,180, 27 ))
G1_PRE_closed=np.zeros((20484,180, 27 ))
G1_POST_closed=np.zeros((20484,180, 27 ))

G0_PRE_open=np.zeros((20484,180, 27 ))
G0_POST_open=np.zeros((20484,180, 27 ))
G0_PRE_closed=np.zeros((20484,180, 27 ))
G0_POST_closed=np.zeros((20484,180, 27 ))


for group  in groups:
    for eye in eyes:
        for sub_iter, subject in enumerate(subjects_fs2meg):
            
    
            meg_subject           = subjects_fs2meg[subject]
            inv       = Conn_utils.subject_inv( meg_subject, group )
            src       = inv['src']
              
                            
            stc_baseline      = mne.read_source_estimate(Conn_utils.Read_conn(meg_subject, 'stc', 'baseline', group, eye, band))
            morph_baseline = mne.compute_source_morph(stc_baseline, subject_from=subject,  subject_to='fsaverage', subjects_dir=subjects_dir)
            stc_baseline_fsaverage = morph_baseline.apply(stc_baseline)
            stc_baseline_fsaverage.crop(0, None)
            x_pre=stc_baseline_fsaverage.bin(width=1)
 
            
            stc_postexpo2     = mne.read_source_estimate(Conn_utils.Read_conn(meg_subject, 'stc', 'postexpo2', group, eye, band))
            morph_post = mne.compute_source_morph(stc_postexpo2, subject_from=subject,  subject_to='fsaverage', subjects_dir=subjects_dir)
            stc_post2_fsaverage = morph_post.apply(stc_postexpo2)
            stc_post2_fsaverage .crop(0, None)
            x_post=stc_post2_fsaverage.bin(width=1)
           
                 
            if group ==1:
                
                if eye== 'open':
                    
                    G1_PRE_open[:,:,sub_iter] = x_pre.data
                    G1_POST_open[:,:,sub_iter]= x_post.data
                else:
                   G1_PRE_closed[:,:,sub_iter] = x_pre.data
                   G1_POST_closed[:,:,sub_iter]= x_post.data
                   
            elif group==0:
            
                if eye== 'open':
                        G0_PRE_open[:,:,sub_iter] = x_pre.data
                        G0_POST_open[:,:,sub_iter]= x_post.data
                else:
                       G0_PRE_closed[:,:,sub_iter] = x_pre.data
                       G0_POST_closed[:,:,sub_iter]= x_post.data
                  

        print('########__subject %s is DONE__########' %subject)
 
    
    ############### NEXT STEP#############
    
# You can decides how to structure data
# default is as below
        
             
DATA= np.zeros((20484,180, 27, 8 )) # number of vertics; times; subjets, conditions
   
DATA[:,:,:, 0]=    G1_PRE_open
DATA[:,:,:, 1]=    G1_PRE_closed 
DATA[:,:,:, 2]=    G1_POST_open
DATA[:,:,:, 3]=    G1_POST_closed
DATA[:,:,:, 4]=    G0_PRE_open
DATA[:,:,:, 5]=    G0_PRE_closed
DATA[:,:,:, 6]=    G0_POST_open
DATA[:,:,:, 7]=    G0_POST_closed           

#np.save('stc_morphed_pre_post2_ G0_G1', DATA)
'''
one data is saved with structure like this
         
DATA[:,:,:, 0]=    PRE_G1
DATA[:,:,:, 1]=    PRE_G0  
DATA[:,:,:, 2]=    POST_G1
DATA[:,:,:, 3]=    POST_G0   
if you are interested to load it; just execute
'''
# loading the data
#DATA=np.load('stc_morphed_pre_post2_ G0_G1.npy')


##############  ANOVA###################
# change parameters

from mne.stats import (spatio_temporal_cluster_test, f_threshold_mway_rm,
                       f_mway_rm, summarize_clusters_stc)

DATA = np.transpose(DATA, [2, 1, 0, 3])  
DATA = [np.squeeze(x) for x in np.split(DATA, 4, axis=-1)] 
factor_levels = [2, 2]

# Tell the ANOVA not to compute p-values which we don't need for clustering
return_pvals = False

# a few more convenient bindings
effects= 'A:B'
n_times = DATA[0].shape[1]
n_conditions = 4
n_subjects=  DATA[0].shape[0]

def stat_fun(*args):
    # get f-values only.
    return f_mway_rm(np.swapaxes(args, 1, 0), factor_levels=factor_levels,
                     effects=effects, return_pvals=return_pvals)[0]
    
    

fvql, pvql= mne.stats.f_mway_rm(np.swapaxes(DATA, 1, 0), factor_levels, effects=effects, correction=False, return_pvals=True)

print('Computing connectivity.')
connectivity = mne.spatial_src_connectivity(src)

#    Now let's actually do the clustering. Please relax, on a small
#    notebook and one single thread only this will take a couple of minutes ...
pthresh = 0.0005 # qttention
f_thresh = f_threshold_mway_rm(n_subjects, factor_levels, effects, pthresh)

#    To speed things up a bit we will ...
n_permutations = 100  # ... run fewer permutations (reduces sensitivity)

print('Clustering...')
T_obs, clusters, cluster_p_values, H0 = spatio_temporal_cluster_test(DATA,  n_jobs=1,
                                 threshold=f_thresh, stat_fun=stat_fun,
                                 n_permutations=n_permutations,
                                 buffer_size=None)
#    Now select the clusters that are sig. at p < 0.05 (note that this value
#    is multiple-comparisons corrected).
good_cluster_inds = np.where(cluster_p_values < 0.05)[0]
       