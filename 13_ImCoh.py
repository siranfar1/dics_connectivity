'''
spectral connectivity : Imaginary Coherency
'''

import mne
import numpy as np
import os 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/05_Scripts')
from config import eyes, runs, subjects_dir, reg, n_jobs, max_sensor_dist, sfreq, bands
import Conn_utils
from mne.viz import plot_sensors_connectivity
wd=os.getcwd()
if wd !='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF':
    os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF')

from mne.minimum_norm import  apply_inverse_epochs
from mne.connectivity import spectral_connectivity , seed_target_indices, phase_slope_index
from mne.viz import circular_layout, plot_connectivity_circle


# Defining the parameters
labels = mne.read_labels_from_annot('fsaverage', parc='aparc', subjects_dir=subjects_dir)
del labels[-1]
labels_colors= [label.color for label in labels]
label_names = [label.name for label in labels]
fmin = 8
fmax = 12
band= 'Alpha'
method= 'imcoh'
eye= 'closed'
session=1
group= 0
i=0
data= list()

# select the subject you wanna process; you can also import subjects from config file


subjects_fs2meg = {  
'2017_11_07_BRAIN_RF_01_CCOS':'brain_rf01_s01',
'2017_11_15_BRAIN_RF_03_ACHA':'brain_rf03_s03',
'2017_12_13_BRAIN_RF_06_YBUR':'brain_rf06_s06',
'2017_12_12_BRAIN_RF_07_RNOE':'brain_rf07_s07',
'2017_12_22_BRAIN_RF_08_MCUV':'brain_rf08_s08',
'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15',
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20',
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26',
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28',
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}

for eye in eyes:

        
    for subject in subjects_fs2meg:
        
        
        if not subject in subjects_fs2meg.keys() : raise Exception('fs_subject not found in subjects_fs2meg')
        subjects_fs2meg.keys()
        meg_subject  = subjects_fs2meg[subject]
    
    
        # load the data
        inv       = Conn_utils.subject_inv( meg_subject, session )
        src       = inv['src']
        fname     = [ Conn_utils.raw_fname(meg_subject, session , run ) for run in runs]
        raw_run1  =mne.io.read_raw_fif(fname[0])
        raw_run2  =mne.io.read_raw_fif(fname[1])
        raw_run9  =mne.io.read_raw_fif(fname[2])
        raw_run10 =mne.io.read_raw_fif(fname[3])
        raw_run11 =mne.io.read_raw_fif(fname[4])
        raw_run12 =mne.io.read_raw_fif(fname[5])
    
        # combine the runs depending on your purpose
        raw_pre= raw_run1.copy()
        raw_pre.append(raw_run2)
        
        raw_post1 =raw_run9.copy()
        raw_post1.append(raw_run10)
        
        raw_post2 =raw_run11.copy()
        raw_post2.append(raw_run12)
    
    
        info        = mne.io.read_info(fname[0])
        picks       = mne.pick_types(info, meg=True, eeg=False, stim=False)
        
            
        # reading epochs
        epochss_baseline , events_baseline  = Conn_utils.epoch_rf(raw_pre, picks=picks)
        
        epochss_postexpo1, events_postexpo1 = Conn_utils.epoch_rf(raw_post1, picks=picks)
        
        epochss_postexpo2, events_postexpo2 = Conn_utils.epoch_rf(raw_post2, picks=picks)
    
        
        # for experiment I just focus on eyes closed condition and take 8,12 Hz as alpha
    
            
        epochs_baseline                    =epochss_baseline[eye]
        epochs_postexpo1                   =epochss_postexpo1[eye]
        epochs_postexpo2                   =epochss_postexpo2[eye]
    
        # compute stc
        snr = 3.0
        lambda2 = 1.0 / snr ** 2
        stcs_baseline  = apply_inverse_epochs(epochs_baseline, inv, lambda2=lambda2, method='dSPM', pick_ori="normal", return_generator= True)
        stcs_postexpo1 = apply_inverse_epochs(epochs_postexpo1, inv, lambda2=lambda2, method='dSPM', pick_ori="normal",  return_generator= True)
        stcs_postexpo2 = apply_inverse_epochs(epochs_postexpo2, inv, lambda2=lambda2, method='dSPM', pick_ori="normal", return_generator= True)
    
        
       
        label_ts_baseline  = mne.extract_label_time_course (stcs_baseline, labels, inv['src'], mode='mean_flip')
        label_ts_postexpo1 = mne.extract_label_time_course (stcs_postexpo1, labels, inv['src'], mode='mean_flip')
        label_ts_postexpo2 = mne.extract_label_time_course (stcs_postexpo2, labels, inv['src'], mode='mean_flip')
        
        
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'stc_ImCoh', 'baseline', session, eye, band), label_ts_baseline)
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'stc_ImCoh', 'postexpo1', session, eye, band), label_ts_postexpo1)
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'stc_ImCoh', 'postexpo2', session, eye, band), label_ts_postexpo2)
      
        #  Compute all-to-all connectivity between labels
        con_baseline, freqs, times, n_epochs, n_tapers  = spectral_connectivity (label_ts_baseline, method=method, mode='multitaper', sfreq=sfreq,fmin=fmin, fmax=fmax, faverage=True, mt_adaptive=True)
        con_postexpo1, freqs, times, n_epochs, n_tapers = spectral_connectivity (label_ts_postexpo1, method=method, mode='multitaper', sfreq=sfreq,fmin=fmin, fmax=fmax, faverage=True, mt_adaptive=True)
        con_postexpo2, freqs, times, n_epochs, n_tapers = spectral_connectivity (label_ts_postexpo2, method=method, mode='multitaper', sfreq=sfreq,fmin=fmin, fmax=fmax, faverage=True, mt_adaptive=True)
    
    
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'con_ImCoh', 'baseline', session, eye, band), con_baseline[:,:,0])
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'con_ImCoh', 'postexpo1', session, eye, band), con_postexpo1[:,:,0])
        np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ImCoh/'+ Conn_utils.CSD_fname(meg_subject, 'con_ImCoh', 'postexpo2', session, eye, band), con_postexpo2[:,:,0])
      
        
        
        data[i]= [con_baseline[:, :, 0], con_postexpo1[:, :, 0], con_postexpo2[:, :, 0], eye, band, group]
        i +=1
        
        
#        plot_connectivity_circle(con_baseline[:, :, 0], labels, label_names,n_lines= 100)
#        plot_connectivity_circle(con_postexpo1[:, :, 0], labels, label_names, node_colors=labels_colors, n_lines= 100)
#        plot_connectivity_circle(con_postexpo2[:, :, 0], labels, label_names, node_colors=labels_colors, n_lines= 100)
#        
#        cluster= [con_baseline[:, :, 0],con_postexpo2[:, :, 0]]
#        
#        F_obs, clusters, cluster_pv, H0 = mne.stats.permutation_cluster_test( cluster, n_permutations=5000, verbose=True, n_jobs=n_jobs)
#        
#    
    
    ##    plot_sensors_connectivity('None' ,con[:, :, 0])
    #    
    #    
    #    from mne.viz import set_3d_view
    #    fig = mne.viz.plot_alignment(subject='sample', subjects_dir=subjects_dir,
    #                         trans=Conn_utils.trans_fname( subject ) , surfaces='white', src=src)
    #    set_3d_view(fig, focalpoint=(0., 0., 0.06))
        
    #    import matplotlib.pyplot as plt
    #    from mne.connectivity import envelope_correlation
    #    label_ts = mne.extract_label_time_course(
    #    stcs_baseline, labels, inv['src'], return_generator=True)
    #    corr = envelope_correlation(label_ts, verbose=True)
    #    
    #    # let's plot this matrix
    #    fig, ax = plt.subplots(figsize=(4, 4))
    #    ax.imshow(corr, cmap='viridis', clim=np.percentile(corr, [5, 95]))
    #    fig.tight_layout()
    #
    #    threshold_prop = 0.05  # percentage of strongest edges to keep in the graph
    #    degree = mne.connectivity.degree(corr, threshold_prop=threshold_prop)
    #    stc = mne.labels_to_stc(labels, degree)
    #    stc = stc.in_label(mne.Label(inv['src'][0]['vertno'], hemi='lh') +
    #                   mne.Label(inv['src'][1]['vertno'], hemi='rh'))
    #    brain = stc.plot(
    #    clim=dict(kind='percent', lims=[75, 85, 95]), colormap='gnuplot',
    #    subjects_dir=subjects_dir, views='dorsal', hemi='both',
    #    smoothing_steps=25, time_label='Alpha band')