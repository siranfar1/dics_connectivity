
'''
################ to calculate connectivity ################
set your working directory
change parameters in the config.py based on your need
'''

import numpy as np
import mne
from mne.time_frequency import read_csd, pick_channels_csd
import conpy
import sys
import os
sys.path.insert(1, '05_Scripts')
import Conn_utils
from matplotlib import pyplot as plt
from config import subjects, subjects_dir, n_jobs, eyes, bands, reg, base, main

os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')




subjects_fs2meg = {

'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15', 
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20', 
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26', # SVD error for 6 files 
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28', 
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}

figure=True
session=1

for subject in subjects_fs2meg:
    print(' ______________ subject %s is being processed __________ ' %subject)
    if not subject in subjects_fs2meg.keys() : raise Exception('fs_subject not found in subjects_fs2meg')
    subjects_fs2meg.keys()
    meg_subject  = subjects_fs2meg[subject]
    
    
    
    # Read the restricted forward model 
    fwd_r = mne.read_forward_solution(base +'_'+ subject  +'-fwd.fif' )
    
    # Convert the forward model to one that defines two orthogonal dipoles at each
    # source, that are tangential to a sphere.
    fwd_tan = conpy.forward_to_tangential(fwd_r)
    
    pairs = np.load('pairs to compare subjects.npy')
    
    fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=main+'freesurfer', n_jobs=n_jobs, add_dist=False)
    fsaverage_to_subj = conpy.utils.get_morph_src_mapping( fsaverage, fwd_tan['src'], indices=True, subjects_dir=main+'freesurfer')[0]
    pairs = [[fsaverage_to_subj[v] for v in pairs[0]],
    [fsaverage_to_subj[v] for v in pairs[1]]]
    
    for eye in eyes :
        
        for band in bands:

            # Read the CSD matrix for baseline
             csd_baseline          = read_csd(Conn_utils.CSD_fname( meg_subject, 'CSD', 'Baseline', session, eye, band))
             csd_baseline          = pick_channels_csd(csd_baseline, fwd_tan['info']['ch_names'])
             con_baseline          = conpy.dics_connectivity( vertex_pairs=pairs, fwd=fwd_tan, data_csd=csd_baseline, reg=reg,n_jobs=n_jobs,)
             con_baseline.save(Conn_utils.CSD_fname( meg_subject, 'connectivity', 'Baseline', session, eye, band))
             
             
             csd_postexpo1         = read_csd(Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo1', session, eye, band))
             csd_postexpo1         = pick_channels_csd(csd_postexpo1, fwd_tan['info']['ch_names'])
             con_postexpo1         = conpy.dics_connectivity( vertex_pairs=pairs, fwd=fwd_tan, data_csd=csd_postexpo1, reg=reg,n_jobs=n_jobs,)
             con_postexpo1.save(Conn_utils.CSD_fname( meg_subject, 'connectivity', 'postexpo1', session, eye, band))
             
             
             csd_postexpo2         = read_csd(Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo2', session, eye, band))
             csd_postexpo2         = pick_channels_csd(csd_postexpo2, fwd_tan['info']['ch_names'])
             con_postexpo2         = conpy.dics_connectivity( vertex_pairs=pairs, fwd=fwd_tan, data_csd=csd_postexpo2, reg=reg,n_jobs=n_jobs,)
             con_postexpo2.save(Conn_utils.CSD_fname( meg_subject, 'connectivity', 'postexpo2', session, eye, band))


             if figure==True:
                 adj = (con_baseline - con_postexpo2).get_adjacency()
                 fig = plt.figure()
                 plt.imshow(adj.toarray(), interpolation='nearest')
                 

                
                
    print(' ______________ subject %s is DONE __________ ' %subject)
             
             
             
    
    
