
"""
Average the power 
"""


import numpy as np
import scipy.io as sio
import mne
import sys
sys.path.insert(1, 'Connectivity/Scripts')
import Conn_utils
from config import subjects, eyes, bands, subjects_fs2meg
import os
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')

# set which condition you wanna retrieve the data for, write 1 for REAL condition and write 2
subjects_dir='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/freesurfer'

group=0 # Chose betzeen 1 or 0, 
# Attention, Here we take the average based on sha, or exposure not sessions

STCS_baseline  = list()
STCS_postexpo1 = list()
STCS_postexpo2 = list()


for eye in eyes:
    
    for band in bands:
        
        
        stcs_baseline      = list()
            
        stcs_postexpo1     = list()
        
        stcs_postexpo2     = list()
            
        for subject in subjects:
            try:
                meg_subject           = subjects_fs2meg[subject]
                
                subjects_dir          ='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/freesurfer'

                stc_csd_baseline      = mne.read_source_estimate(Conn_utils.Read_conn(meg_subject, 'stc', 'baseline', group, eye, band))
                
                stc_csd_postexpo1     = mne.read_source_estimate(Conn_utils.Read_conn(meg_subject, 'stc', 'postexpo1', group, eye, band))
                
                stc_csd_postexpo2     = mne.read_source_estimate(Conn_utils.Read_conn(meg_subject, 'stc', 'postexpo2', group, eye, band))
                
                
                
                 # Morph the STC to the fsaverage brain, we have three sets of Connectivity pre/post1/post2.
                stc_csd_baseline.subject   = subject
                #stc_baseline_fsaverage     = stc_csd_baseline.morph('stc_csd_baseline', subjects_dir=subjects_dir)
                morph                      =  mne.compute_source_morph(stc_csd_baseline, subject_from=subject ,subject_to='fsaverage', subjects_dir=subjects_dir)
                stc_baseline_fsaverage     =  morph.apply(stc_csd_baseline)
                stcs_baseline.append(stc_baseline_fsaverage)
                
                
                stc_csd_postexpo1.subject  = subject
                #stc_postexpo1_fsaverage    = stc_csd_postexpo1.morph('fsaverage', subjects_dir=subjects_dir)
                morph1                     = mne.compute_source_morph(stc_csd_postexpo1, subject_from=subject ,subject_to='fsaverage', subjects_dir=subjects_dir)
                stc_postexpo1_fsaverage    =  morph1.apply(stc_csd_postexpo1)
                stcs_postexpo1.append(stc_postexpo1_fsaverage)
                
                
                stc_csd_postexpo2.subject  = subject
                #stc_postexpo2_fsaverage    = stc_csd_postexpo2.morph('fsaverage', subjects_dir=subjects_dir)
                morph2                     = mne.compute_source_morph(stc_csd_postexpo2, subject_from=subject ,subject_to='fsaverage', subjects_dir=subjects_dir)
                stc_postexpo2_fsaverage    =  morph2.apply(stc_csd_postexpo2)
                stcs_postexpo2.append(stc_postexpo2_fsaverage)
            except:
                print('Subject %s is missing' %subject)
            
            
     # Average the source estimates for baseline
    data_baseline    = np.mean([stc.data for stc in stcs_baseline], axis=0)
    ga_stc_baseline  = mne.SourceEstimate(data_baseline, vertices=stcs_baseline[0].vertices, tmin=stcs_baseline[0].tmin, tstep=stcs_baseline[0].tstep)
    ga_stc_baseline.save('averaged power of all subs_baseline_group_%s_eyes_%s_%s'  %(group, eye, band))
    sio.savemat('averaged power of all subs_baseline_group_%s_eyes_%s_%s.mat'  %(group, eye, band),ga_stc_baseline)
    STCS_baseline.append(ga_stc_baseline) # I dont need it I guess
    
    
     # Average the source estimates for postexpo1
    data_postexpo1      = np.mean([stc.data for stc in stcs_postexpo1], axis=0)
    ga_stc_postexpo1    = mne.SourceEstimate(data_postexpo1, vertices=stcs_postexpo1[0].vertices, tmin=stcs_postexpo1[0].tmin, tstep=stcs_postexpo1[0].tstep)
    ga_stc_postexpo1.save('averaged power of all subs_postexpo1_group_%s_eyes_%s_%s'  %(group, eye, band))
    sio.savemat('averaged power of all subs_postexpo1_group_%s_eyes_%s_%s.mat'  %(group, eye, band))
    STCS_postexpo1.append(ga_stc_postexpo1) # Here I am not sure if I'm right
    
    
      # Average the source estimates for baseline
    data_postexpo2      = np.mean([stc.data for stc in stcs_postexpo2], axis=0)
    ga_stc_postexpo2    = mne.SourceEstimate(data_postexpo2, vertices=stcs_postexpo2[0].vertices, tmin=stcs_postexpo2[0].tmin, tstep=stcs_postexpo2[0].tstep)
    ga_stc_postexpo2.save('averaged power of all subs_postexpo2_group_%s_eyes_%s_%s'  %(group, eye, band))
    sio.savemat('averaged power of all subs_postexpo2_group_%s_eyes_%s_%s.mat'  %(group, eye, band), ga_stc_postexpo2)
    STCS_postexpo2.append(ga_stc_postexpo2) # Here I am not sure if I'm right
    
    


    STC_Diff_base_post1= ga_stc_baseline - ga_stc_postexpo1 # Should this number be normalized? if so, then how?
    STC_Diff_base_post1.save('STCs difference between baseline_postexpo1__group_%s_eyes_%s_%s'  %(group, eye, band))
    
    
    STC_Diff_base_post2= ga_stc_baseline - ga_stc_postexpo2 # Should this number be normalized? if so, then how?
    STC_Diff_base_post2.save('STCs difference between baseline_postexpo2__group_%s_eyes_%s_%s'  %(group, eye, band))

#    brain2 = STC_Diff_base_post1.plot(
#    subject='fsaverage',
#    hemi='split',
#    views='med',
#    background='white',
#    foreground='black',
#    time_label='',
#    time_viewer=True)
#    
#    input('close the figure and press inter to continue:) ')
       
            