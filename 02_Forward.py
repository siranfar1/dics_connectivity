import numpy as np
import mne
import conpy
import os 
import sys
sys.path.insert(1, 'Sepideh/Scripts')
import Conn_utils
from config import n_jobs, subjects_dir, min_pair_dist, max_sensor_dist, subjects
from mayavi import mlab
wd=os.getcwd()
if wd !='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF':
    os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF')



subject0      =  '2017_11_07_BRAIN_RF_01_CCOS'
fwd0          = Conn_utils.subject_fwd(subjects[0])
fwd0          = conpy.restrict_forward_to_sensor_range(fwd0, max_sensor_dist)
fwds          = [fwd0]


for subject in subjects[1:]:
    fwds.append(Conn_utils.subject_fwd( subject))
    
    
fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=subjects_dir, n_jobs=n_jobs, add_dist=False)
vert_inds = conpy.select_shared_vertices(fwds, ref_src=fsaverage, subjects_dir=subjects_dir)

# Restrict all forward operators to the same vertices and save them.
for fwd, vert_ind, subject in zip(fwds, vert_inds, subjects):
    fwd_r = conpy.restrict_forward_to_vertices(fwd, vert_ind)
    mne.write_forward_solution('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Sepideh/Data/restricted_forward'+'_'+ subject  +'-fwd.fif' , fwd_r, overwrite=True)

    if subject == subjects[0]: #updating subject 1 
        fwd0 = fwd_r   


    fig = mne.viz.plot_alignment(fwd['info'], trans= Conn_utils.trans_fname( subject ),  subjects_dir=subjects_dir, src=fwd_r['src'], meg='sensors', surfaces='white')
    fig.scene.background = (1, 1, 1)  # white
    g = fig.children[-1].children[0].children[0].glyph.glyph
    g.scale_factor = 0.008
    mlab.view(135, 120, 0.3, [0.01, 0.015, 0.058])



pairs = conpy.all_to_all_connectivity_pairs(fwd0, min_dist=min_pair_dist) # min_pair_dist is 0.04 btw sensors

# Store the pairs in fsaverage space
subj1_to_fsaverage = conpy.utils.get_morph_src_mapping(fsaverage, fwd0['src'], indices=True, subjects_dir=subjects_dir)[1]
pairs = [[subj1_to_fsaverage[v] for v in pairs[0]],
         [subj1_to_fsaverage[v] for v in pairs[1]]]
np.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Sepideh/Data/pairs to compare subjects', pairs)
