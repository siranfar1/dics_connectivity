%  ANOVA on local efficiency 


%%%%%%%%%%%%% loading G0 %%%%%%%%%%%%%%
pre_local_Eff_G0_open=load('local_Effciency_baseline_Session0_eyesopen.mat')
pre_local_Eff_G0_close=load('local_Effciency_baseline_Session0_eyesclosed.mat')

post_local_Eff_G0_open=load('local_Effciency_post2_Session0_eyesopen.mat')
post_local_Eff_G0_close=load('local_Effciency_post2_Session0_eyesclosed.mat')

inter_open_G0=load('Intercepts_Session0_eyesopen.mat')
inter_close_G0=load('Intercepts_Session0_eyesclosed.mat')

intercepts_G0=[inter_open_G0.intercepts  ;inter_close_G0.intercepts ; inter_open_G0.intercepts  ;inter_close_G0.intercepts];
local_Eff_coef_G0= [pre_local_Eff_G0_open.pre_Eloc'; pre_local_Eff_G0_close.pre_Eloc'; post_local_Eff_G0_open.post_Eloc'  ; post_local_Eff_G0_close.post_Eloc'];

PRE_name= -1;
POST_name= 1;

name_G0= [repelem(PRE_name, size(local_Eff_coef_G0,1)/2)'; repelem( POST_name ,size(local_Eff_coef_G0,1)/2)'];
Y_G0= [intercepts_G0, name_G0];



%%%%%%%%%%%%% loading G1 %%%%%%%%%%%%%%
pre_local_Eff_G1_open=load('local_Effciency_baseline_Session1_eyesopen.mat')
pre_local_Eff_G1_close=load('local_Effciency_baseline_Session1_eyesclosed.mat')

post_local_Eff_G1_open=load('local_Effciency_post2_Session1_eyesopen.mat')
post_local_Eff_G1_close=load('local_Effciency_post2_Session1_eyesclosed.mat')

inter_open_G1=load('Intercepts_Session1_eyesopen.mat')
inter_close_G1=load('Intercepts_Session1_eyesclosed.mat')


intercepts_G1=[inter_open_G1.intercepts  ;inter_close_G1.intercepts ; inter_open_G1.intercepts  ;inter_close_G1.intercepts];
local_Eff_coef_G1= [pre_local_Eff_G1_open.pre_Eloc'; pre_local_Eff_G1_close.pre_Eloc'; post_local_Eff_G1_open.post_Eloc'; post_local_Eff_G1_close.post_Eloc'];
name_G1= [repelem(PRE_name, size(local_Eff_coef_G1,1)/2)'; repelem( POST_name ,size(local_Eff_coef_G1,1)/2)'];
Y_G1= [intercepts_G1, name_G1];






%%%%%%%%%%%%% combining two groups %%%%%%%%

local_Effciency= [local_Eff_coef_G1;local_Eff_coef_G0];
factors= [Y_G1;Y_G0];

for i= 1:size(local_Effciency,2)
 pval(:,i) = anovan(local_Effciency(:,i),factors, 'varnames', {'experiment_time', 'eyes', 'gender', 'sham_real', 'pre_post'});
close;
end

good_idx= find(pval< 0.05);
correction= bonf_holm(pval(good_idx));
significant_p= find(correction < 0.05) 
which_p=pval(good_idx(significant_p));
where= good_idx(significant_p);
which_factor= rem(where, 5) % first factor at nodes 64 , 66 are affected by time
which_node=where/5