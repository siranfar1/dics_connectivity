'''
to prepare data for ANOVA on 'ciplv connectivity'
'''


import mne
import numpy as np
import os 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/05_Scripts')
from config import eyes, runs, subjects_dir, reg, n_jobs, max_sensor_dist, sfreq, bands, main 
import Conn_utils 
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/ciPLV')
from mne.connectivity import spectral_connectivity
from mne.minimum_norm import  apply_inverse_epochs
# select the subject you wanna process; you can also import subjects from config file
            


band= 'Alpha'
groups=[0, 1]# for ciPLV group=session I converted before

G1_PRE_open= np.zeros((68,14,25))
G1_POST_open= np.zeros((68,14, 25))
G1_PRE_closed= np.zeros((68,14,25))
G1_POST_closed= np.zeros((68,14,25))

G0_PRE_open= np.zeros((68,14,25))
G0_POST_open= np.zeros((68,14, 25))
G0_PRE_closed= np.zeros((68,14,25))
G0_POST_closed= np.zeros((68,14,25))
strong_conn=  [6, 7,14, 15, 18, 19,  22, 23,38, 50, 51, 60, 61 ,66]

subjects_fs2meg={
#        '2017_11_07_BRAIN_RF_01_CCOS': 'brain_rf01_s01',
 '2017_11_15_BRAIN_RF_03_ACHA': 'brain_rf03_s03',
 '2017_12_13_BRAIN_RF_06_YBUR': 'brain_rf06_s06',
 '2017_12_12_BRAIN_RF_07_RNOE': 'brain_rf07_s07',
 '2017_12_22_BRAIN_RF_08_MCUV': 'brain_rf08_s08',
 '2018_01_10_BRAIN_RF_09_BTOR': 'brain_rf09_s09',
 '2018_01_16_BRAIN_RF_10_MMAU': 'brain_rf10_s10',
 '2018_02_12_BRAIN_RF_11_PTUR': 'brain_rf11_s11',
 '2018_02_14_BRAIN_RF_12_COGO': 'brain_rf12_s12',
 '2018_02_01_BRAIN_RF_13_CFAV': 'brain_rf13_s13',
 '2018_02_14_BRAIN_RF_14_CBUT': 'brain_rf14_s14',
# '2018_03_09_BRAIN_RF_15_AMOR': 'brain_rf15_s15',
 '2018_02_28_BRAIN_RF_16_DMIC': 'brain_rf16_s16',
 '2018_04_04_BRAIN_RF_17_AAUJ': 'brain_rf17_s17',
 '2018_03_15_BRAIN_RF_18_CCLO': 'brain_rf18_s18',
 '2018_03_09_BRAIN_RF_19_LGIG': 'brain_rf19_s19',
# '2018_04_09_BRAIN_RF_20_PPEI': 'brain_rf20_s20',
# '2018_04_05_BRAIN_RF_21_SMNI': 'brain_rf21_s21',
 '2018_04_09_BRAIN_RF_22_JBEA': 'brain_rf22_s22',
 '2018_04_09_BRAIN_RF_23_MNYO': 'brain_rf23_s23',
 '2018_04_11_BRAIN_RF_24_SYAH': 'brain_rf24_s24',
 '2018_04_11_BRAIN_RF_25_GBER': 'brain_rf25_s25',
 '2018_04_11_BRAIN_RF_26_CRAT': 'brain_rf26_s26',
 '2018_05_03_BRAIN_RF_S27_NNAV': 'brain_rf27_s27',
 '2018_06_07_BRAIN_RF_S28_SPAI': 'brain_rf28_s28',
 '2018_06_11_BRAIN_RF_29_RMOR': 'brain_rf29_s29',
 '2018_06_21_BRAIN_RF_30_SBAS': 'brain_rf30_s30',
 '2018_06_21_BRAIN_RF_31_JFER': 'brain_rf31_s31',
 '2018_07_13_BRAIN_RF_32_EBEN': 'brain_rf32_s32'}

for group in groups:
    
    for eye in eyes:
        
        for sub_iter, subject in enumerate(subjects_fs2meg):         
            
            meg_subject  = subjects_fs2meg[subject] 
            
    #        try:
            conn_pre=np.load(Conn_utils.CSD_fname(meg_subject, 'ciplv', 'baseline', group, eye, band)+ '.npy')
            conn_pre = conn_pre + np.transpose(conn_pre)
            
            conn_post=np.load(Conn_utils.CSD_fname(meg_subject, 'ciplv', 'postexpo2', group, eye, band)+ '.npy')
            conn_post = conn_post + np.transpose(conn_post)
            
            if group ==1:
                
                if eye== 'open':
                    
                    G1_PRE_open[:,:,sub_iter] = conn_pre[:, strong_conn]
                    G1_POST_open[:,:,sub_iter]= conn_post[:, strong_conn]
                else:
                   G1_PRE_closed[:,:,sub_iter] = conn_pre[:, strong_conn]
                   G1_POST_closed[:,:,sub_iter]= conn_post[:, strong_conn]
                   
            elif group==0:
            
                if eye== 'open':
                        G0_PRE_open[:,:,sub_iter] = conn_pre[:, strong_conn]
                        G0_POST_open[:,:,sub_iter]= conn_post[:, strong_conn]
                else:
                       G0_PRE_closed[:,:,sub_iter] = conn_pre[:, strong_conn]
                       G0_POST_closed[:,:,sub_iter]= conn_post[:, strong_conn]
     
#        except:
            print('###############subject %s is DONE###############' %subject)

######################################## PART 2: ANOVA connectivity ################################

DATA= np.zeros((68,14,25,8)) # in matrix format
DATA[:,:,:,0] = G1_PRE_open
DATA[:,:,:,1] = G1_PRE_closed
DATA[:,:,:,2] = G0_PRE_open
DATA[:,:,:,3] = G0_PRE_closed
DATA[:,:,:,4] =G1_POST_open
DATA[:,:,:,5] =G1_POST_closed
DATA[:,:,:,6] =G0_POST_open
DATA[:,:,:,7] =G0_POST_closed


'''
DATA= np.zeros((68,14,25,4)) 

DATA[:,:,:,0]=G1_PRE_closed

DATA[:,:,:,1] =G0_PRE_closed

DATA[:,:,:,2] =G1_POST_closed

DATA[:,:,:,3] =G0_POST_closed
'''

np.save( 'icPLV_subjects_conditions', DATA)     



################################ clustering ############################

from mne.stats import (spatio_temporal_cluster_test, f_threshold_mway_rm,
                       f_mway_rm, summarize_clusters_stc)
DATA = np.transpose(DATA, [2, 1, 0, 3])  
DATA = [np.squeeze(x) for x in np.split(DATA, 8, axis=-1)] 
factor_levels = [2, 2,2]

# Tell the ANOVA not to compute p-values which we don't need for clustering
return_pvals = False

# a few more convenient bindings
effects= 'A:B'
n_times = DATA[0].shape[1]
n_conditions = 8
n_subjects=  DATA[0].shape[0]

def stat_fun(*args):
    # get f-values only.
    return f_mway_rm(np.swapaxes(args, 1, 0), factor_levels=factor_levels,
                     effects=effects, return_pvals=return_pvals)[0]
    
fvql, pvql= mne.stats.f_mway_rm(np.swapaxes(DATA, 1, 0), factor_levels, effects=effects, correction=False, return_pvals=True)



#    Now let's actually do the clustering. Please relax, on a small
#    notebook and one single thread only this will take a couple of minutes ...
pthresh = 0.0005 # qttention
f_thresh = f_threshold_mway_rm(n_subjects, factor_levels, effects, pthresh)

#    To speed things up a bit we will ...
n_permutations = 100  # ... run fewer permutations (reduces sensitivity)

print('Clustering...')
T_obs, clusters, cluster_p_values, H0 = spatio_temporal_cluster_test(DATA,  n_jobs=1,
                                 threshold=f_thresh, stat_fun=stat_fun,
                                 n_permutations=n_permutations,
                                 buffer_size=None)
#    Now select the clusters that are sig. at p < 0.05 (note that this value
#    is multiple-comparisons corrected).
good_cluster_inds = np.where(cluster_p_values < 0.05)[0]
       




'''
for plotting purpose

import matplotlib.pyplot as plt
from mne.viz import plot_connectivity_circle, circular_layout
labels = mne.read_labels_from_annot('fsaverage', parc='aparc', subjects_dir=subjects_dir)
#labels= list( label[i] for i in strong_conn )
del labels[-1]
label_names = [label.name for label in labels]

labels_colors= [label.color for label in labels]
lh_labels = [name for name in label_names if name.endswith('lh')]

# Get the y-location of the label
label_ypos = list()
for name in lh_labels:
    idx = label_names.index(name)
    ypos = np.mean(labels[idx].pos[:, 1])
    label_ypos.append(ypos)

lh_labels = [label for (yp, label) in sorted(zip(label_ypos, lh_labels))]

rh_labels = [label[:-2] + 'rh' for label in lh_labels]
node_order = list()
node_order.extend(lh_labels[::-1])  # reverse the order
node_order.extend(rh_labels)
node_angles = circular_layout(label_names, node_order, start_pos=90, group_boundaries=[0, len(label_names) / 2])

figure= plt.figure( facecolor= 'black')
plt.rcParams['text.color']='white'
plt.suptitle('averaged ciPLV_G0_pre vs post' )
plot_connectivity_circle(np.mean(G0_PRE_closed, axis=2), label_names,  node_angles=node_angles, node_colors=labels_colors,n_lines=150,  subplot=121, title= 'Baseline_eyes closed', fig= figure)
plot_connectivity_circle(np.mean(G0_POST_closed, axis=2), label_names, node_angles=node_angles, node_colors=labels_colors, n_lines=150,  subplot=122, title= 'postexposure2_eyes closed', fig= figure)


plot_connectivity_circle(np.mean(PRE_open, axis=2), label_names,  node_angles=node_angles, node_colors=labels_colors, n_lines=150,  subplot=223, title= 'Baseline_eyes open', fig= figure)
plot_connectivity_circle(np.mean(POST_open, axis=2), label_names, node_angles=node_angles, node_colors=labels_colors, n_lines=150, subplot=224, title= 'postexposure2_eyes open', fig= figure)


'''
