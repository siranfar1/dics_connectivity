
import mne
import conpy
from mne.externals.h5io import write_hdf5
import Conn_utils
from config import subjects, eyes, bands, subjects_fs2meg, main,labels, n_jobs,subjects_dir
import os

os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')



fsaverage = mne.setup_source_space('fsaverage', spacing='ico4', subjects_dir=main+'freesurfer', n_jobs=n_jobs, add_dist=False)
conditions= ['Baseline', 'postexpo1', 'postexpo2']      
eyecondition= 'eyecondition'
freband= 'alphaband'
blind= 'blind'
pre = 'pre'
post1= 'post1'
post2= 'post2'

del labels[-1]
cons   = dict()
groups =[0, 1]
cons[blind]= list()
cons[eyecondition]=list()
cons[freband]= list()
cons[pre]= list()
cons[post1]=list()
cons[post2]= list()
    
for group in groups:
    
    for eye in eyes:
        
        for band in bands:
           
                
                for subject in subjects:
                    try:
                        meg_subject= subjects_fs2meg[subject]
                        con1_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[0] , group, eye, band))
                        con2_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[1] , group, eye, band))
                        con3_subject = conpy.read_connectivity(Conn_utils.Read_conn(meg_subject, 'connectivity', conditions[2] , group, eye, band))
                    
                        # Morph the Connectivity to the fsaverage brain. This is possible, since the original source space was fsaverage morphed to the current subject.
                        con1_fsaverage = con1_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
                        con1_parc = con1_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
                        
                        con2_fsaverage = con2_subject.to_original_src(fsaverage, subjects_dir=subjects_dir)
                        con2_parc = con2_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
                        
                        con3_fsaverage= con3_subject.to_original_src(fsaverage, subjects_dir= subjects_dir)
                        con3_parc = con3_fsaverage.parcellate(labels, summary='degree', weight_by_degree=False)
                        
                        # By now, the connection objects should define the same connection pairs between the same vertices.
    
                        cons[pre].append(con1_parc)
                        cons[post1].append(con2_parc)
                        cons[post2].append(con3_parc)
                        cons[eyecondition].append(eye)
                        cons[freband].append(band)
                        cons[blind].append(group)
                        
    
                
                
                    except:
                        missings=subject
                        print('subject %s is missing' %subject)
                
                
                
                
                
                
                
                
                
     