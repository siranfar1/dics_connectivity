
"""
Power mapping using DICS.
"""

import mne

import os 
import sys
import os.path as op
#sys.path.insert(1, 'Sepideh/Scripts')
import Conn_utils
from config import eyes,  subjects, bands,  base_dir
from mayavi import mlab
from mne.time_frequency import read_csd
#wd=os.getcwd()
#if wd !='/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF':
#    os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF')
os.chdir('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity')

session=2
subjects_fs2meg = {  
'2017_11_07_BRAIN_RF_01_CCOS':'brain_rf01_s01',
'2017_11_15_BRAIN_RF_03_ACHA':'brain_rf03_s03',
'2017_12_13_BRAIN_RF_06_YBUR':'brain_rf06_s06',
'2017_12_12_BRAIN_RF_07_RNOE':'brain_rf07_s07',
'2017_12_22_BRAIN_RF_08_MCUV':'brain_rf08_s08',
'2018_01_10_BRAIN_RF_09_BTOR':'brain_rf09_s09',
'2018_01_16_BRAIN_RF_10_MMAU':'brain_rf10_s10',
'2018_02_12_BRAIN_RF_11_PTUR':'brain_rf11_s11',
'2018_02_14_BRAIN_RF_12_COGO':'brain_rf12_s12',
'2018_02_01_BRAIN_RF_13_CFAV':'brain_rf13_s13',
'2018_02_14_BRAIN_RF_14_CBUT':'brain_rf14_s14',
'2018_03_09_BRAIN_RF_15_AMOR':'brain_rf15_s15',
'2018_02_28_BRAIN_RF_16_DMIC':'brain_rf16_s16',
'2018_04_04_BRAIN_RF_17_AAUJ':'brain_rf17_s17',
'2018_03_15_BRAIN_RF_18_CCLO':'brain_rf18_s18',
'2018_03_09_BRAIN_RF_19_LGIG':'brain_rf19_s19',
'2018_04_09_BRAIN_RF_20_PPEI':'brain_rf20_s20',
'2018_04_05_BRAIN_RF_21_SMNI':'brain_rf21_s21',
'2018_04_09_BRAIN_RF_22_JBEA':'brain_rf22_s22',
'2018_04_09_BRAIN_RF_23_MNYO':'brain_rf23_s23',
'2018_04_11_BRAIN_RF_24_SYAH':'brain_rf24_s24',
'2018_04_11_BRAIN_RF_25_GBER':'brain_rf25_s25',
'2018_04_11_BRAIN_RF_26_CRAT':'brain_rf26_s26',
'2018_05_03_BRAIN_RF_S27_NNAV':'brain_rf27_s27',
'2018_06_07_BRAIN_RF_S28_SPAI':'brain_rf28_s28',
'2018_06_11_BRAIN_RF_29_RMOR':'brain_rf29_s29',
'2018_06_21_BRAIN_RF_30_SBAS':'brain_rf30_s30',
'2018_06_21_BRAIN_RF_31_JFER':'brain_rf31_s31',
'2018_07_13_BRAIN_RF_32_EBEN':'brain_rf32_s32'
}


 # Do you want to plot the stc for each subject? change figure to True/False
Figure='False'
for subject in subjects_fs2meg:
    if not subject in subjects_fs2meg.keys() : raise Exception('fs_subject not found in subjects_fs2meg')
    subjects_fs2meg.keys()
    meg_subject  = subjects_fs2meg[subject]
    
    
    
#for subject in subjects:
#    meg_subject  = subjects_fs2meg[subject]
    try:
    # Read the forward model
        fwd     = Conn_utils.read_fwd( subject)
       
        # Read the info structure
        info    = mne.io.read_info( op.join(base_dir, meg_subject, 'subject_meanhp.fif' ) )
        info    = mne.pick_info(info, mne.pick_types(info, meg='grad'))
    
    
    # Compute source power for alpha frequency bands and eyes open/closed
        for eye in eyes :
            
            for band in bands:
    
                # Read the CSD matrix
                 csd_baseline          = read_csd(Conn_utils.CSD_fname( meg_subject, 'CSD', 'Baseline', session, eye, band))
                 stc_csd_baseline      = Conn_utils.stc_csd(info, csd_baseline, eye, band, fwd)
                 stc_csd_baseline.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' +Conn_utils.CSD_fname(meg_subject, 'stc', 'baseline', session, eye, band))
                 
                 csd_postexpo1         = read_csd(Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo1', session, eye, band))
                 stc_csd_postexpo1     = Conn_utils.stc_csd(info, csd_postexpo1, eye, band, fwd)
                 stc_csd_postexpo1.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' +Conn_utils.CSD_fname(meg_subject, 'stc', 'postexpo1', session, eye, band))
                 
                 csd_postexpo2         = read_csd(Conn_utils.CSD_fname(meg_subject, 'CSD', 'postexpo2', session, eye, band))
                 stc_csd_postexpo2     = Conn_utils.stc_csd(info, csd_postexpo2, eye, band, fwd)
                 stc_csd_postexpo2.save('/network/lustre/iss01/cenir/analyse/meeg/BRAIN_RF/Connectivity/' + Conn_utils.CSD_fname(meg_subject, 'stc', 'postexpo2', session, eye, band))
                
    
       
                # Plot difference in power between the two experimental conditions,
    #             if Figure=='Ture':
    #                 stc_contrast = stc_csd_baseline - stc_csd_postexpo2
    #                 figs = []
    #                 fig = mlab.figure()
    #                 mlab.clf()
    #                 brain = stc_contrast.plot(subject=subject, hemi='both', timeviewer= True, 
    #                                           title='stc difference between baseline and postexpo2 for %s , eyes %s ' % (band, eye), figure=fig)
    #                 mlab.view(-90, 110, 420, [0, 0, 0], figure=fig)
    #                 figs.append(fig)
                    
    except:
        print(' ______________ subject %s is ,missing __________ ' %subject)
print(' ______________ subject %s is DONE __________ ' %subject)